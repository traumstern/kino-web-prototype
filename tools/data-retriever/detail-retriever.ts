import fs from "node:fs";
import { Detail } from "../../generated/api-types.js";

export const defaultImage = "assets/kino-titel-poster.png";

type MovieData = {
  id: string;
  titel: string;
  zusatz: string;
  header: string;
  beschreibung: string;
};

export async function fetchDetails(
  apiBaseUrl: string,
  outputDir: string,
  apiKey: string | undefined,
) {
  const movies = (await (
    await fetch(`${apiBaseUrl}/movies`)
  ).json()) as MovieData[];

  movies.forEach(async ({ id, ...movie }) => {
    fs.writeFileSync(
      `${outputDir}/${id}.json`,
      JSON.stringify(
        {
          ...defaultMovie,
          ...(await searchData(movie.titel.toLowerCase(), apiKey)),
          titleDe: movie.titel,
          press: [
            movie.header.trim(),
            movie.zusatz.trim(),
            movie.beschreibung.trim(),
          ]
            .join("\r\n")
            .trim(),
        },
        null,
        2,
      ),
    );
  });
}

const defaultMovie = {
  titleDe: "",
  image: defaultImage,
  website: "#",
  trailer: [
    {
      link: "#",
      type: "youtube",
    },
  ],
  director: "",
  duration: "",
  press: "",
};

async function searchData(
  title: string,
  apiKey: string | undefined,
): Promise<{} | Partial<Detail>> {
  if (!apiKey) {
    console.warn("API_KEY_OMDB missing. Proceeding without searching OMDB");
    return {};
  }
  try {
    const result = (await (
      await fetch(
        `http://www.omdbapi.com/?t=${encodeURIComponent(
          title,
        )}&apikey=${apiKey}`,
      )
    ).json()) as any;
    if ("Error" in result) {
      console.warn(result, title);
      return {};
    }
    return Object.fromEntries(
      Object.entries({
        duration: result.Runtime,
        director: result.Director,
        image: imageOrDefault(result.Poster),
        website: result.Website,
        press: result.Plot,
      }).filter(([key, value]) => value !== undefined),
    );
  } catch (err) {
    console.error(err);
    return {};
  }
}

function imageOrDefault(imageUrl: string) {
  if (imageUrl === "N/A") {
    return defaultImage;
  }
  return imageUrl;
}
