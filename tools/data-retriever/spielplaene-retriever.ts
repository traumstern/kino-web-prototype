import dayjs from "dayjs";
import fs from "node:fs";

type Date = { date: string; movies: string[] };
export async function fetchSpielplaene(
  apiBaseUrl: string,
  outputFolder: string,
) {
  const dates = (await (await fetch(`${apiBaseUrl}/dates`)).json()) as Date[];
  const clusters: { [key: string]: Date[] } = {};
  for (const item of dates) {
    const date = dayjs(item.date);
    const week = `${date.year()}-${date.week()}`;
    if (!clusters[week]) {
      clusters[week] = [];
    }
    clusters[week].push(item);
  }
  Object.entries(clusters).forEach(([id, dates]) => {
    fs.writeFileSync(
      `${outputFolder}/${id}.json`,
      JSON.stringify(dates, null, 2),
    );
  });
}
