import dotenv from "dotenv";
import dayjs from "dayjs";
import "../src/de-kino.js";
import weekOfYear from "dayjs/plugin/weekOfYear.js";
import { fetchDetails } from "./data-retriever/detail-retriever.js";
import { fetchSpielplaene } from "./data-retriever/spielplaene-retriever.js";
import { validateRequiredConfigs } from "./config.js";
dayjs.extend(weekOfYear);

dotenv.config();
const { API_BASE_URL, DETAIL_FOLDER, SPIELPLAN_FOLDER } =
  validateRequiredConfigs();

await fetchDetails(API_BASE_URL, DETAIL_FOLDER, process.env.API_KEY_OMDB);
await fetchSpielplaene(API_BASE_URL, SPIELPLAN_FOLDER);
