import {
  DetailRepository,
  SpielplanRepository,
  TemplateRepository,
  isEmptyWeek,
  SpielplanPerWeek,
} from "./infrastructure/index.js";
import { compileDetailTemplate, renderDetailPages } from "./detail-pages.js";
import Handlebars from "handlebars";
import { compileSpielplanTemplate, renderSpielplan } from "./spielplan-page.js";
import { FilmRepository } from "./infrastructure/film-repository.js";
import { compileFilmeTemplate, renderFilme } from "./filme-page.js";
import dayjs from "dayjs";
import "./de-kino.js";
import weekOfYear from "dayjs/plugin/weekOfYear.js";
dayjs.extend(weekOfYear);

export async function generate({
  templateRepository,
  spielplanRepository,
  detailRepository,
  filmRepository,
}: GeneratorOptions) {
  // Make sure to use the German timezone
  process.env.TZ = "Europe/Berlin";

  setupTemplateEngine(templateRepository);

  const spielplaene = await getUpcomingSpielplaene(spielplanRepository);
  const movieKeys = detailRepository.findAllIds(spielplaene);
  const movieDetails = Object.assign(
    {},
    ...(await Promise.all(
      movieKeys.map(async (detailId) => ({
        [detailId]: {
          detailLink: `${detailId}.html`,
          ...(await detailRepository.getDetail(detailId)),
        },
      })),
    )),
  );
  detailRepository.storeRenderedPages(
    renderDetailPages(
      movieKeys,
      (detailId) => movieDetails[detailId],
      compileDetailTemplate(templateRepository),
    ),
  );

  spielplanRepository.storeRendered(
    renderSpielplan(
      spielplaene,
      (detailId) => movieDetails[detailId],
      compileSpielplanTemplate(templateRepository),
    ),
  );

  filmRepository.storeRendered(
    renderFilme(
      compileFilmeTemplate(templateRepository),
      Object.values(movieDetails),
    ),
  );
}

export function setupTemplateEngine(templates: TemplateRepository) {
  const layout = Handlebars.compile(
    templates.read(`frame.handlebars`).toString(),
  );
  Handlebars.registerPartial("layout", layout);
  failOnMissingProps();
}

export function failOnMissingProps() {
  Handlebars.registerHelper("helperMissing", (...args) => {
    throw new HandlebarsMissingVariableError(args[args.length - 1].name);
  });
}

export function resetBehaviorOnMissingProps() {
  Handlebars.unregisterHelper("helperMissing");
}

async function getUpcomingSpielplaene(
  spielplanRepository: SpielplanRepository,
): Promise<SpielplanPerWeek[]> {
  // TODO throw if every Spielplan is empty
  //   page generation should fail, admins need to be notified
  const numOfWeeksToFetch = 4;
  const startOfCurrentWeek = dayjs().startOf("week");
  return Promise.all(
    Array.from(Array(numOfWeeksToFetch).keys())
      .map(async (index) => {
        const week = startOfCurrentWeek.add(index, "week");
        return spielplanRepository.getSpielplanPerWeek(
          week.year(),
          week.week(),
        );
      })
      .filter(async (spielplanPerWeek) => !isEmptyWeek(await spielplanPerWeek)),
  );
}
export class HandlebarsMissingVariableError extends Error {
  constructor(public readonly helperName: string) {
    super(`Missing prop "${helperName}"`);
  }
}
export type GeneratorOptions = {
  templateRepository: TemplateRepository;
  spielplanRepository: SpielplanRepository;
  detailRepository: DetailRepository;
  filmRepository: FilmRepository;
};
