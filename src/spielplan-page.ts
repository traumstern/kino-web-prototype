import {
  Spielplan,
  SpielplanPerWeek,
  TemplateRepository,
  WeekStr,
} from "./infrastructure/index.js";
import Handlebars from "handlebars";
import dayjs from "dayjs";
import "./de-kino.js";
import { Detail, MoviesJson } from "../generated/api-types.js";

export function compileSpielplanTemplate(
  templateRepository: TemplateRepository,
): HandlebarsTemplateDelegate {
  const emptySpielplan = Handlebars.compile(
    templateRepository.read(`empty-spielplan.component.handlebars`).toString(),
  );
  Handlebars.registerPartial("empty-spielplan", emptySpielplan);
  return Handlebars.compile(
    templateRepository.read("spielplan.component.handlebars").toString(),
  );
}

export function renderSpielplan(
  spielplaene: SpielplanPerWeek[],
  fetchDetail: (key: string) => Detail,
  template: HandlebarsTemplateDelegate,
): string {
  const spielplanWithTransformedDays = spielplaene.map((spielplanPerWeek) => ({
    week: spielplanPerWeek.week,
    spielplaene: spielplanPerWeek.spielplaene.map((spielplan) =>
      transformDays(spielplan, fetchDetail),
    ),
  }));
  const spielplanPageWeeks = spielplanWithTransformedDays.map(
    ({ week, spielplaene }) => ({
      week: toHumanReadableWeek(week),
      dates: spielplaene,
    }),
  );
  return template({
    weeks: spielplanPageWeeks,
    titleDe: "Spielplan",
    isSpielplanOrFilme: true,
    isSpielplan: true,
  });
}

export type SpielplanPageWithHumanReadableDates = {
  week: HumanReadableWeekStr;
  dates: MoviesOnDate[];
}[];

export type MoviesOnDate = {
  date: HumanReadableDateStr;
  dayName: HumanReadableDayName;
  movies: (Detail & {
    detailLink: string;
  })[];
};

export type HumanReadableWeekStr = `Woche: ${string} bis ${string}`;
export type HumanReadableDateStr = `${number}.${number}.`;
export type HumanReadableDayName = string;

function toHumanReadableWeek(week: WeekStr): HumanReadableWeekStr {
  const [year, w] = week.split("-").map((s) => parseInt(s));
  const weekDate = dayjs().set("year", year).week(w);
  const startOfWeek = weekDate.startOf("week");
  const endOfWeek = weekDate.endOf("week");
  return `Woche: ${startOfWeek.format("D.M.")} bis ${endOfWeek.format("D.M.")}`;
}

function transformDays(
  spielplan: Spielplan,
  fetchDetail: (movieKey: string) => Detail,
) {
  return transformDay(spielplan.date, spielplan.movies, fetchDetail);
}

function transformDay(
  dateStr: string,
  movies: MoviesJson[],
  fetchDetail: (movieKey: string) => Detail,
) {
  const date = dayjs(dateStr);
  return {
    date: date.format("D.M.") as HumanReadableDateStr,
    dayName: date.format("dddd") as HumanReadableDayName,
    movies: movies
      .map((movie) => ({
        detailLink: `${movie.id}.html`,
        startTime: movie.startTime,
        ...fetchDetail(movie.id),
      }))
      .sort((a, b) => a.startTime.localeCompare(b.startTime)),
  };
}

function mapObjectValues<T extends { [key: string]: T[keyof T] }, R>(
  object: T,
  mapFn: (originalValue: T[keyof T]) => R,
): { [key in keyof T]: R } {
  return Object.fromEntries(
    Object.entries(object).map(([key, value]) => [key, mapFn(value)]),
  ) as { [key in keyof T]: R };
}

function typeSafeObjectEntries<T extends Object>(
  obj: T,
): [keyof T, T[keyof T]][] {
  return Object.entries(obj) as [keyof T, T[keyof T]][];
}
