import dayjs from "dayjs";
import de_locale from "dayjs/locale/de.js";

/**
 * This locale represents cinema weeks in Germany.
 * Cinema weeks start on Thursday. Whenever we
 * want to work with cinema weeks, we can
 * simply use the locale "de-kino".
 * @example
 *   import dayjs from "dayjs";
 *   import "./de-kino.js";
 *   dayjs().startOf("week").format("YYYY-MMMM-DD") // represents Thursday
 */
const locale = {
  ...de_locale,
  name: "de-kino",
  weekStart: 4,
};

dayjs.locale("de-kino", locale);
export default locale;
