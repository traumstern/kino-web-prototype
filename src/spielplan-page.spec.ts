import {
  failOnMissingProps,
  resetBehaviorOnMissingProps,
} from "./generator.js";
import { expect } from "chai";
import { compileSpielplanTemplate, renderSpielplan } from "./spielplan-page.js";
import { parseJsonFile } from "./infrastructure/json-file-parser.js";
import { fixtureBasePath, templateBaseDir } from "./config.js";
import {
  FileBasedTemplateRepository,
  SpielplanPerWeek,
} from "./infrastructure/index.js";
import { Detail } from "../generated/api-types.js";

describe("spielplan-page", () => {
  before(() => {
    failOnMissingProps();
  });
  after(() => {
    resetBehaviorOnMissingProps();
  });
  it("renders a spielplan with a single movie", () => {
    const spielplan = [{ ...defaultSpielplan }];
    const rendered = renderSpielplan(
      spielplan,
      (_) => defaultContent,
      template,
    );
    expect(rendered).to.contain("Woche: 4.1. bis 10.1.");
    expect(rendered).to.contain("Donnerstag");
    expect(rendered).to.contain("Die Kairo Verschwörung");
  });

  for (const emptySpielplan of [
    {
      week: "2024-01",
      spielplaene: [],
    },
    {
      week: "2024-01",
      spielplaene: [{ date: "2024-04-01", movies: [] }],
    },
  ] as SpielplanPerWeek[]) {
    it(`displays a default message, when an empty spielplan is given (${JSON.stringify(
      emptySpielplan,
    )})`, () => {
      const rendered = renderSpielplan(
        [emptySpielplan],
        (_) => defaultContent,
        template,
      );
      expect(rendered).to.contain("Aktuell kein Spielplan");
    });
  }
});

const defaultSpielplan: SpielplanPerWeek = {
  week: "2024-01",
  spielplaene: [
    {
      date: "2024-01-04",
      movies: [{ id: "movie-1", startTime: "16:30" }],
    },
  ],
};
const defaultContent = parseJsonFile<Detail>(
  `${fixtureBasePath}/detail/movie-boy-from-heaven.json`,
);
const template = compileSpielplanTemplate(
  new FileBasedTemplateRepository(templateBaseDir),
);
