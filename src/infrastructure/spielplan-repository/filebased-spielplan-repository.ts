import { parseJsonFileAndValidate } from "../json-file-parser.js";
import fs from "node:fs";
import {
  SpielplanRepository,
  SpielplanRepositoryError,
  WeekStr,
  toValidWeekString,
  InvalidWeekStringError,
  SpielplanPerWeek,
  Spielplan,
} from "./spielplan-repository.js";
import path from "node:path";

export class FileBasedSpielplanRepository implements SpielplanRepository {
  constructor(
    private spielplanFolderPath: string,
    private outputFile: string,
  ) {}

  async getSpielplaenePerWeek(): Promise<SpielplanPerWeek[]> {
    return this.parseAllFiles(this.spielplanFolderPath);
  }

  async getSpielplanPerWeek(
    year: number,
    cinemaWeekOfYear: number,
  ): Promise<SpielplanPerWeek> {
    const week: WeekStr = `${year}-${cinemaWeekOfYear}`;
    const path = `${this.spielplanFolderPath}/${week}.json`;
    if (!fs.existsSync(path)) {
      return {
        week: week,
        spielplaene: [],
      };
    }
    return {
      week,
      spielplaene: await parseJsonFileAndValidate<Spielplan[]>(
        path,
        import("../../json-spec/spielplan.json", { with: { type: "json" } }),
      ),
    };
  }

  private async parseAllFiles(folderPath: string): Promise<SpielplanPerWeek[]> {
    const files = this.enumerateFiles(folderPath);
    const jsonFiles: string[] = files.filter(
      (file) => path.extname(file) === ".json",
    );
    return Promise.all(
      jsonFiles.map(async (file: string) => {
        try {
          return {
            week: toValidWeekString(path.basename(file, path.extname(file))),
            spielplaene: await parseJsonFileAndValidate<Spielplan[]>(
              `${folderPath}/${file}`,
              import("../../json-spec/spielplan.json", {
                with: { type: "json" },
              }),
            ),
          };
        } catch (err) {
          if (err instanceof InvalidWeekStringError) {
            throw new FilenameNotWeekStrError(file);
          }
          throw err;
        }
      }),
    );
  }

  private enumerateFiles(folderPath: string): string[] {
    this.validateBaseDir(folderPath);
    return fs.readdirSync(folderPath);
  }

  private validateBaseDir(folderPath: string): void {
    if (!fs.existsSync(folderPath)) {
      throw new SpielplanBaseDirMissingError(folderPath);
    }
    if (!fs.lstatSync(folderPath).isDirectory()) {
      throw new SpielplanBaseDirIsNoDirectoryError(folderPath);
    }
  }

  storeRendered(renderedSpielplan: string): void {
    fs.writeFileSync(`${this.outputFile}`, renderedSpielplan);
  }
}

export class SpielplanBaseDirInvalidError extends SpielplanRepositoryError {
  constructor(
    message: string,
    public readonly path: string,
  ) {
    super(message);
  }
}
export class SpielplanBaseDirIsNoDirectoryError extends SpielplanBaseDirInvalidError {
  constructor(path: string) {
    super("The specified path is a file but must be a folder.", path);
  }
}

export class SpielplanBaseDirMissingError extends SpielplanBaseDirInvalidError {
  constructor(path: string) {
    super("The specified path does not exist.", path);
  }
}

export class FilenameNotWeekStrError extends Error {
  constructor(public readonly filenamePrefix: string) {
    super("Invalid filename encountered. Does not match WeekStr");
  }
}
