import { MoviesJson } from "../../../generated/api-types.js";

export interface SpielplanRepository {
  getSpielplaenePerWeek(): Promise<SpielplanPerWeek[]>;
  getSpielplanPerWeek(
    year: number,
    cinemaWeekOfYear: number,
  ): Promise<SpielplanPerWeek>;
  storeRendered(renderedSpielplan: string): void;
}

export type WeekStr = `${number}-${number}`;
export function isWeekStr(candidate: string): candidate is WeekStr {
  return /^[0-9]+-[0-9]+$/.test(candidate);
}

export function toValidWeekString(candidate: string): WeekStr {
  if (isWeekStr(candidate)) {
    return candidate;
  }
  throw new InvalidWeekStringError(candidate);
}
export class InvalidWeekStringError extends Error {
  constructor(public readonly invalidWeekString: string) {
    super("Invalid WeekStr encountered");
  }
}
export type DateStr = `${number}-${number}-${number}`;

export type Spielplan =
  | {
      date: DateStr;
      movies: MoviesJson[];
    }
  | EmptySpielplan;

export type SpielplanPerWeek =
  | {
      week: WeekStr;
      spielplaene: Spielplan[];
    }
  | EmptySpielplanPerWeek;

export type EmptySpielplan = {
  date: DateStr;
  movies: [];
};

export type EmptySpielplanPerWeek = {
  week: WeekStr;
  spielplaene: [] | EmptySpielplan[];
};

export function isEmptyWeek(
  spielplanPerWeek: SpielplanPerWeek,
): spielplanPerWeek is EmptySpielplanPerWeek {
  if (spielplanPerWeek.spielplaene.length === 0) {
    return true;
  }
  return spielplanPerWeek.spielplaene.every((item) => item.movies.length === 0);
}

export class SpielplanRepositoryError extends Error {}
