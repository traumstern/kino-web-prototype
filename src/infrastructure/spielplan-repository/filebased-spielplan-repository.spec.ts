import { fixtureBasePath } from "../../config.js";
import chaiAsPromised from "chai-as-promised";
import * as Chai from "chai";
const chai = Chai.use(chaiAsPromised);
const expect = chai.expect;
import {
  FileBasedSpielplanRepository,
  FilenameNotWeekStrError,
  SpielplanBaseDirIsNoDirectoryError,
  SpielplanBaseDirMissingError,
} from "./filebased-spielplan-repository.js";

describe("FileBasedSpielplanRepository", () => {
  it("retrieves all spielplaene for a folder", async () => {
    const repository = new FileBasedSpielplanRepository(
      `${fixtureBasePath}/spielplan-repo/2-files-4-dates-5-movies`,
      "/dev/stdout",
    );
    expect(await repository.getSpielplaenePerWeek()).to.have.length(2);
  });
  it("fails for invalid file names in folder", async () => {
    const repository = new FileBasedSpielplanRepository(
      `${fixtureBasePath}/spielplan-repo/with-invalid-files`,
      "/dev/stdout",
    );
    await expect(repository.getSpielplaenePerWeek()).to.be.rejectedWith(
      FilenameNotWeekStrError,
    );
  });
  it("fails for non-existing folders", async () => {
    const repository = new FileBasedSpielplanRepository(
      `${fixtureBasePath}/non-existing-folder`,
      "/dev/stdout",
    );
    await expect(repository.getSpielplaenePerWeek()).to.be.rejectedWith(
      SpielplanBaseDirMissingError,
    );
  });
  it("fails for non-folders", async () => {
    const repository = new FileBasedSpielplanRepository(
      "/dev/stdin",
      "/dev/stdout",
    );
    await expect(repository.getSpielplaenePerWeek()).to.be.rejectedWith(
      SpielplanBaseDirIsNoDirectoryError,
    );
  });
  it("returns an empty array when there are no spielplaene", async () => {
    const repository = new FileBasedSpielplanRepository(
      `${fixtureBasePath}/spielplan-repo/without-jsons`,
      "/dev/stdout",
    );
    expect(await repository.getSpielplaenePerWeek()).to.have.length(0);
  });
  it("returns an empty spielplan for the requested week, when requested spielplan is not available", async () => {
    const repository = new FileBasedSpielplanRepository(
      `${fixtureBasePath}/spielplan-repo/without-jsons`,
      "/dev/stdout",
    );
    expect(await repository.getSpielplanPerWeek(2023, 999)).to.deep.equal({
      week: "2023-999",
      spielplaene: [],
    });
  });
  it("gets the correct spielplan for a week", async () => {
    const repository = new FileBasedSpielplanRepository(
      `${fixtureBasePath}/spielplan-repo/2-files-4-dates-5-movies`,
      "/dev/stdout",
    );
    const year = 2023;
    const week = 46;
    const weekStr = `${year}-${week}`;
    const spielplan = await repository.getSpielplanPerWeek(year, week);
    expect(spielplan).to.deep.equal({
      week: weekStr,
      spielplaene: [
        {
          date: "2023-11-12",
          movies: [{ id: "2023-11-12-movie", startTime: "18:30" }],
        },
        {
          date: "2023-11-13",
          movies: [{ id: "2023-11-13movie", startTime: "16:00" }],
        },
      ],
    });
  });
});
