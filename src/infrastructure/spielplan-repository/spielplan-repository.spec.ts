import { expect } from "chai";
import {
  InvalidWeekStringError,
  isEmptyWeek,
  isWeekStr,
  Spielplan,
  toValidWeekString,
  WeekStr,
} from "./spielplan-repository.js";
import { MoviesJson } from "../../../generated/api-types.js";
describe("spielplan-repository", () => {
  describe("isWeekStr", () => {
    for (const matchingString of ["2023-2", "23-2", "3-2"])
      it(`should correctly validate WeekStr format of ${matchingString}`, () => {
        expect(isWeekStr(matchingString)).to.be.true;
      });
    for (const invalidString of ["3", "3-abc", "abc", "", "2023-3-1"]) {
      it(`should return false for invalid string ${invalidString}`, () => {
        expect(isWeekStr(invalidString)).to.be.false;
      });
    }
  });

  describe("isEmpty", () => {
    for (const emptyPlanPerWeek of [
      { week: "2024-01" as WeekStr, spielplaene: [] as Spielplan[] },
      {
        week: "2024-01" as WeekStr,
        spielplaene: [
          { date: "2024-01-04", movies: [] as MoviesJson[] },
        ] as Spielplan[],
      },
    ]) {
      it(`detects empty SpielplaenePerWeek for ${JSON.stringify(
        emptyPlanPerWeek,
      )}`, () => {
        expect(isEmptyWeek(emptyPlanPerWeek)).to.be.true;
      });
    }
    it("considers SpielplanPerWeek not empty when there are movies", () => {
      expect(
        isEmptyWeek({
          week: "2024-01",
          spielplaene: [
            {
              date: "2024-01-04",
              movies: [{ id: "movie", startTime: "16:30" }],
            },
          ],
        }),
      ).to.be.false;
    });
  });
  describe("toValidWeekString function tests", () => {
    it("should return a valid week string when a valid week string is passed", () => {
      expect(toValidWeekString("2023-1")).to.equal("2023-1");
    });

    it("should throw an error when an invalid week string is passed", () => {
      expect(() => toValidWeekString("invalid")).to.throw(
        InvalidWeekStringError,
      );
    });
  });
});
