import * as fs from "node:fs";

export interface FilmRepository {
  storeRendered(content: string): void;
}

export class FileBasedFilmRepository implements FilmRepository {
  constructor(private outputBasePath: string) {}

  storeRendered(content: string): void {
    fs.writeFileSync(`${this.outputBasePath}/filme.html`, content);
  }
}
