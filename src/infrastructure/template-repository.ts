import * as fs from "node:fs";

export interface TemplateRepository {
  read(path: string): string;
}

export class FileBasedTemplateRepository implements TemplateRepository {
  constructor(private templateBaseDir: string) {}

  read(path: string): string {
    return fs.readFileSync(`${this.templateBaseDir}/${path}`).toString();
  }
}
