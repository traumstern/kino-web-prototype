import * as fs from "node:fs";
import { createValidatorFromSchemaObject } from "../validation/validator-factory.js";
import { ErrorObject, ValidationError } from "ajv";
import { SchemaObject } from "ajv/dist/2019.js";

export function parseJsonFile<T>(path: string): T {
  return JSON.parse(fs.readFileSync(path).toString());
}

export async function parseJsonFileAndValidate<T>(
  path: string,
  schema: Promise<SchemaObject>,
): Promise<T> {
  const parsed = parseJsonFile<T>(path);
  const validate = await createValidatorFromSchemaObject<T>(await schema);
  const valid = validate(parsed);
  if (valid) {
    return parsed;
  }
  throw new ValidationErrorWithPath(validate.errors ?? [], path);
}

export class ValidationErrorWithPath extends ValidationError {
  constructor(
    errors: Partial<ErrorObject>[],
    public readonly path: string,
  ) {
    super(errors);
  }
}
