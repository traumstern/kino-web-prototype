import * as fs from "node:fs";

export function prepareFolderStructure(outputBasePath: string) {
  if (!fs.existsSync(outputBasePath)) {
    fs.mkdirSync(outputBasePath, {
      recursive: true,
    });
  }
}
