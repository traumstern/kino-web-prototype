import { RenderedDetailPage } from "../../detail-pages.js";
import fs from "node:fs";
import { Detail } from "../../../generated/api-types.js";
import { parseJsonFileAndValidate } from "../json-file-parser.js";
import { SpielplanPerWeek } from "../spielplan-repository/spielplan-repository.js";
import { DetailId, DetailRepository } from "./detail-repository.js";

export class FileBasedDetailRepository implements DetailRepository {
  constructor(
    private detailFolder: string,
    private outputBasePath: string,
  ) {}

  storeRenderedPages(details: RenderedDetailPage[]) {
    for (const detail of details) {
      fs.writeFileSync(
        `${this.outputBasePath}/${detail.id}.html`,
        detail.content,
      );
    }
  }

  async getDetail(id: DetailId): Promise<Detail> {
    return await parseJsonFileAndValidate<Detail>(
      `${this.detailFolder}/${id}.json`,
      import("../../json-spec/detail.json", { with: { type: "json" } }),
    );
  }

  findAllIds(spielplanWeeks: SpielplanPerWeek[]): DetailId[] {
    return spielplanWeeks
      .map((spielplanPerWeek) =>
        spielplanPerWeek.spielplaene
          .map((plan) => plan.movies.map((movie) => movie.id))
          .flat(),
      )
      .flat();
  }
}
