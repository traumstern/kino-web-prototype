import { RenderedDetailPage } from "../../detail-pages.js";
import { SpielplanPerWeek } from "../spielplan-repository/spielplan-repository.js";
import { Detail } from "../../../generated/api-types.js";

export type DetailId = string;

export interface DetailRepository {
  storeRenderedPages(details: RenderedDetailPage[]): void;
  getDetail(id: DetailId): Promise<Detail>;
  findAllIds(spielplanJson: SpielplanPerWeek[]): DetailId[];
}
