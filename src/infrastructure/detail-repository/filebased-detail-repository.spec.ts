import { fixtureBasePath } from "../../config.js";
import chaiAsPromised from "chai-as-promised";
import * as Chai from "chai";
import { ValidationError } from "ajv";
import { FileBasedDetailRepository } from "./filebased-detail-repository.js";
const chai = Chai.use(chaiAsPromised);
const expect = chai.expect;

describe("filebased-detail-repository", () => {
  const repo = new FileBasedDetailRepository(
    `${fixtureBasePath}/detail`,
    "/dev/stdout",
  );
  it("returns detail from a valid json", async () => {
    expect(await repo.getDetail("movie-boy-from-heaven")).to.deep.equal(
      (
        await import(`${fixtureBasePath}/detail/movie-boy-from-heaven.json`, {
          with: { type: "json" },
        })
      ).default,
    );
  });
  it("throws for invalid json", async () => {
    await expect(repo.getDetail("invalid")).to.be.rejectedWith(ValidationError);
  });
});
