import dayjs from "dayjs";
import "./de-kino.js";
import weekOfYear from "dayjs/plugin/weekOfYear.js";
import { expect } from "chai";
dayjs.extend(weekOfYear);
describe("dayjs - de-kino", () => {
  it("considers Thursday as the start of week", () => {
    expect(dayjs().startOf("week").format("dddd")).to.equal("Donnerstag");
  });
  it("calculates Calendarweeks based on Thursdays", () => {
    // Thursday is on 2024-01-04
    expect(dayjs("2024-01-01").week()).to.equal(53);
    expect(dayjs("2024-01-04").week()).to.equal(1);
  });
});
