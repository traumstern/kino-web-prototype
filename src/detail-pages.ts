import Handlebars from "handlebars";
import { DetailId, TemplateRepository } from "./infrastructure/index.js";

export function compileDetailTemplate(
  templateRepository: TemplateRepository,
): HandlebarsTemplateDelegate {
  return Handlebars.compile(
    templateRepository.read("film-detail.component.handlebars").toString(),
  );
}

export function renderDetailPages(
  detailIds: DetailId[],
  parseJsonFile: (detailPath: string) => Record<string, any>,
  template: HandlebarsTemplateDelegate,
): RenderedDetailPage[] {
  return detailIds.map((id) => ({
    id: id,
    content: template({ ...parseJsonFile(id) }),
  }));
}

export type RenderedDetailPage = { id: string; content: string };
