import Ajv2019, { SchemaObject } from "ajv/dist/2019.js";
import addFormats from "ajv-formats";

export async function createValidatorFromSchemaImport<T>(file: string) {
  const schema = await import(file, { with: { type: "json" } });
  return createValidatorFromSchemaObject<T>(schema);
}

export async function createValidatorFromSchemaObject<T>(
  schema: SchemaObject,
): Promise<Ajv2019.ValidateFunction<T>> {
  const ajv = initAjv();
  return await ajv.compileAsync<T>(schema);
}

function initAjv() {
  const ajv = new Ajv2019.default({
    // loadSchema dynamically resolves $ref, but only works with compileAsync!
    loadSchema: async (uri: string) => {
      return await import(`../json-spec/${uri}`, { with: { type: "json" } });
    },
    allErrors: true,
  });
  addFormats.default(ajv);
  return ajv;
}
