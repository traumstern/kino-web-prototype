import { fileURLToPath } from "url";
import path from "path";
import dotenv from "dotenv";
dotenv.config();
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const requiredConfigKeys = [
  "OUTPUT_BASE_PATH",
  "DETAIL_FOLDER",
  "SPIELPLAN_FOLDER",
] as const;
type RequiredConfigs = { [k in (typeof requiredConfigKeys)[number]]: string };

export function validateRequiredConfigs(): RequiredConfigs {
  const unsatisfiedConfigs = requiredConfigKeys.filter(
    (requiredConfig) => !(requiredConfig in process.env),
  );
  if (unsatisfiedConfigs.length > 0) {
    throw new UnsatisfiedConfigsError(unsatisfiedConfigs);
  }
  return requiredConfigKeys.reduce(
    (acc, config) => ({
      ...acc,
      [config]: process.env[config],
    }),
    {},
  ) as RequiredConfigs;
}

export const templateBaseDir = `${__dirname}/../templates`;
export const fixtureBasePath = `${__dirname}/fixtures`;

class UnsatisfiedConfigsError extends Error {
  constructor(public readonly unsatisfiedConfigs: string[]) {
    super(`Configs missing`);
  }
}
