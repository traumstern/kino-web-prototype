import { TemplateRepository } from "./infrastructure/index.js";
import Handlebars from "handlebars";
import { Detail } from "../generated/api-types.js";

export function compileFilmeTemplate(
  templateRepository: TemplateRepository,
): HandlebarsTemplateDelegate {
  return Handlebars.compile(
    templateRepository.read("filme.component.handlebars").toString(),
  );
}

export function renderFilme(
  template: HandlebarsTemplateDelegate,
  movieDetails: (Detail & { detailLink: string })[],
): string {
  return template({
    titleDe: "Filme & Veranstaltungen",
    isSpielplanOrFilme: true,
    isFilme: true,
    movies: movieDetails,
  });
}
