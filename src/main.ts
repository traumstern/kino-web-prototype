import {
  prepareFolderStructure,
  FileBasedSpielplanRepository,
  FileBasedTemplateRepository,
} from "./infrastructure/index.js";
import { generate } from "./generator.js";
import { FileBasedFilmRepository } from "./infrastructure/film-repository.js";
import { FileBasedDetailRepository } from "./infrastructure/index.js";
import { templateBaseDir, validateRequiredConfigs } from "./config.js";

async function main() {
  const { OUTPUT_BASE_PATH, SPIELPLAN_FOLDER, DETAIL_FOLDER } =
    validateRequiredConfigs();
  prepareFolderStructure(OUTPUT_BASE_PATH);
  await generate({
    templateRepository: new FileBasedTemplateRepository(templateBaseDir),
    spielplanRepository: new FileBasedSpielplanRepository(
      SPIELPLAN_FOLDER,
      `${OUTPUT_BASE_PATH}/index.html`,
    ),
    detailRepository: new FileBasedDetailRepository(
      DETAIL_FOLDER,
      OUTPUT_BASE_PATH,
    ),
    filmRepository: new FileBasedFilmRepository(OUTPUT_BASE_PATH),
  });
}

await main();
