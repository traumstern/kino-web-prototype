import { compileDetailTemplate, renderDetailPages } from "./detail-pages.js";
import { FileBasedTemplateRepository } from "./infrastructure/index.js";
import { fixtureBasePath, templateBaseDir } from "./config.js";
import { expect } from "chai";
import {
  failOnMissingProps,
  HandlebarsMissingVariableError,
  resetBehaviorOnMissingProps,
} from "./generator.js";
import { parseJsonFile } from "./infrastructure/json-file-parser.js";
import Handlebars from "handlebars";

describe("detail-pages", () => {
  before(() => {
    failOnMissingProps();
  });
  after(() => {
    resetBehaviorOnMissingProps();
  });
  it("renders a single detail page", () => {
    const content = { ...defaultContent };
    const rendered = renderDetailPages(["my-id"], (_) => content, template);
    expect(rendered).to.have.length(1);
    assertRendered(content, rendered[0].content);
  });
  it("fails when a property is missing", () => {
    const { titleDe = undefined, ...contentWithoutTitleDe } = {
      ...defaultContent,
    };
    expect(() =>
      renderDetailPages(["my-id"], (_) => contentWithoutTitleDe, template),
    ).to.throw(HandlebarsMissingVariableError);
  });
});

function assertRendered(content: Record<string, any>, rendered: string) {
  const { trailer, ...primitiveValues } = content;
  for (const key of Object.keys(primitiveValues)) {
    expect(rendered, `${key} not rendered as expected`).to.contain(
      Handlebars.Utils.escapeExpression(primitiveValues[key]),
    );
  }
  trailer.forEach((item: any) =>
    expect(rendered).to.contain(Handlebars.Utils.escapeExpression(item.link)),
  );
}

const defaultContent = parseJsonFile<Record<string, any>>(
  `${fixtureBasePath}/detail/movie-boy-from-heaven.json`,
);
const template = compileDetailTemplate(
  new FileBasedTemplateRepository(templateBaseDir),
);
